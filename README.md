# Cirkus
## An open sourced Hugo theme

**Supports**:
- Bulma
- Sass
- Formspree

**Features**:
- Mobile first and responsiveness
- Multilanguage support
- Hero image
- Contact page
- Portifolio

### Updates

The following command will update the theme:

`$ hugo mod get -u gitlab.com/lsrdg/cirkus`

### License

Copyright © lsrdg. Distributed under the MIT license.
